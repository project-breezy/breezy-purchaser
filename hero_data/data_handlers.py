import os

def OpenTemplate():
    # Open the Purchaser class file.
    file = open("purchaser.lua", "r")

    # Store all of the file's text in a string.
    text = file.read()

    # Close the file pointer.
    file.close()

    # Return the text string.
    return text

def GetJsonList():
    # Get the list of all files in the json
    # directory and return them.
    return os.listdir("./json")

def ConvertListToString(inList):
    # Convert the items list to a string
    # with the square braces removed.
    text = str(inList)[1:-1:1]

    # Replace all of the single quotes
    # with double quotes for consistency.
    text = text.replace("'", '"')
    
    # Return the modified string.
    return text

    
