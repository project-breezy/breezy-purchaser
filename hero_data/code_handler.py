
def GenerateCondition(cond):
    if cond == "fullall":
        return "if self.HeroHandler:GetNumItemsInInventory() == 9 then\n"
    if cond == "full":
        return "if self.HeroHandler:GetNumItemsInInventory() >= 6 then\n"
    if cond == "empty":
        return "if self.HeroHandler:GetNumItemsInInventory() == 0 then\n"
    if cond.startswith("i"):
        try:
            int(cond[1:])
        except:
            raise TypeError("An Inventory Size condition must have a valid number.")
        return "if self.HeroHandler:GetNumItemsInInventory() == " + cond[1:] + " then\n"
    if cond.startswith("p"):
        try:
            int(cond[1:])
        except:
            raise TypeError("A Purchase condition must have a valid number.")
        return "if self.CurrentItem == " + cond[1:] + " then\n"
    raise ValueError("Condition generation has failed on token:", cond)    


def GenerateAction(action, item):
    body  = "\t\tlocal item = self.HeroHandler:FindItemInInventory(\"" + item + "\")\n"
    if action == "use":    
        body += "\t\tself.HeroHandler:CastAbilityOnTarget(self.HeroHandler, item, 0)\n"
        return body
    if action == "remove":
        body += "\t\tself.HeroHandler:RemoveItem(item)\n"
        return body
    if action == "drop":
        body += "\t\tself.HeroHandler:DropItemAtPositionImmediate(item, self.HeroHandler:GetAbsOrigin())\n"
        return body
    raise ValueError("Action generation has failed on token:", action, item)

def GenerateCode(commands):
    # condition action item_name
    # Conditions:
    #       full    - if regular inventory is full
    #       fullall - if inventory + backpack is full
    #       empty   - if inventory is empty
    #       iX      - if the inventory has X items
    #       pX      - if the next item is purchase number X

    # Actions:
    #       use     - use the item 
    #       remove  - delete the item from the inventory
    #       drop    - drop the item on the ground
    code = ""
    
    for line in commands:
        tokens = line.split()

        condition = tokens[0]
        action    = tokens[1]
        name      = tokens[2]

        first = "\t" + GenerateCondition(condition)
        body  = GenerateAction(action, name)
        end   = "\tend\n\n"

        code += first + body + end

    code += "\treturn true"

    return code
