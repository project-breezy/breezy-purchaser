--------------------------------------------------------------------------------------
-- Class: R_HERO_TITLE_NAME Purchaser
--
-- An interface for providing purchasing functionality for R_HERO_TITLE_NAME. 
--
-- @author Robert Smith
--------------------------------------------------------------------------------------

-- Import any other required files.
require("bots/util/utility")

-- Create a table for holding all class features.
local Purchaser = {}

-- Initialize any properties necessary for execution.
Purchaser.HeroName           = "R_HERO_NAME"
Purchaser.AbilityCount       = R_ABILITY_COUNT
Purchaser.ItemCount          = R_ITEM_COUNT

Purchaser.ItemList           = {R_ITEM_LIST}
Purchaser.AbilityList        = {R_ABILITY_LIST}

Purchaser.CurrentItem        = 1
Purchaser.CurrentAbility     = 1

Purchaser.HeroHandler        = nil

-- Change the hero's inventory as necessary before an item purchase.
function Purchaser:ManageInventory()
R_MANAGE_INVENTORY_CODE
end

-- Set the hero handler so we can make changes to a hero directly later.
function Purchaser:SetHeroHandler(hero)
    self.HeroHandler = hero
end

-- Attempt to buy the next ability in the hero's ability list.
function Purchaser:BuyNextAbility()
    -- If we haven't set a hero handler, we fail negatively.
    if self.HeroHandler == nil then
        do return -1 end
    end
    
    -- If we don't have enough ability upgrade points, we fail negatively.
    if self.HeroHandler:GetAbilityPoints() < 1 then
        do return -2 end
    end
    
    -- If we've already purchased all abilities and talents, we fail positively.
    if self.CurrentAbility > self.AbilityCount then
        do return 2 end
    end
        
    -- Since we haven't failed, we start by getting the ability from the handler.
    local ability = self.HeroHandler:FindAbilityByName(self.AbilityList[self.CurrentAbility])
    
    -- If we couldn't find the ability, we fail negatively.
    if ability == nil then
        do return -3 end
    end
    
    -- We found the ability, so we can upgrade it by one level.
    self.HeroHandler:UpgradeAbility(ability)
    
    -- Increase the current ability counter by 1.
    self.CurrentAbility = self.CurrentAbility + 1

    -- Return positively.
    return 1
end

-- Attempt to buy the next item in the hero's ability list.
function Purchaser:BuyNextItem()
    -- If we haven't set a hero handler, we fail negatively.
    if self.HeroHandler == nil then
        do return -1 end
    end

    -- If we've already purchased all items, we fail positively.
    if self.CurrentItem > self.ItemCount then
        do return 2 end
    end

     -- Get the name of the item to be purchased.
    local item = self.ItemList[self.CurrentItem]
    
    -- Get the cost of the item to be purchased.
    local cost = GetItemCost(item)

    -- If we can't afford the next item, we fail negatively.
    if cost < self.HeroHandler:GetGold() then
        do return -2 end
    end

    -- Start by managing the hero's inventory as necessary.
    -- Most functions after this function assumes the hero's
    -- inventory has been properly set up with this function.
    self:ManageInventory()    
    
    -- If the hero still doesn't have space for the item, we fail negatively.
    if self.HeroHandler:GetNumItemsInInventory() > 5 then
        do return -3 end
    end
       
    -- Spend the required gold to purchase the item.
    self.HeroHandler:SpendGold(cost, DOTA_ModifyGold_PurchaseItem)
    
    -- Add the item to the hero's inventory.
    self.HeroHandler:AddItemByName(item)
      
    -- Increase the current item counter by 1.
    self.CurrentItem = self.CurrentItem + 1

    -- Return positively.
    return 1
end

-- Return a table of the hero's previously purchased items in order.
function Purchaser:GetItemPurchaseHistory()
    -- Create a local history table.    
    local history = {}
 
    -- Get every item from the Item List that has been 
    -- purchased so far and add it to the history table.
    for i=1, self.CurrentItem-1, 1 do
        history[i] = self.ItemList[i]
    end
    
    -- Return the item history table.
    return history
end

-- Return a table of the hero's previously purchased abilities in order.
function Purchaser:GetAbilityPurchaseHistory()
    -- Create a local history table.    
    local history = {}
 
    -- Get every ability from the Ability List that has been 
    -- purchased so far and add it to the history table.
    for i=1, self.CurrentAbility-1, 1 do
        history[i] = self.AbilityList[i]
    end
    
    -- Return the item history table.
    return history
end

-- Return the table for this action. This becomes
-- accessible on a "require" call to this file.
return Purchaser
