# Breezy Purchaser

#### BIG NOTE: THIS FEATURE HAS ALREADY BEEN INTEGRATED INTO BREEZY ADDON, BUT THE INFORMATION HERE SHOULD BE RELEVANT FOR GENERATING ITEM AND ABILITY PURCHASE LISTS!

Breezy Purchaser is a pre-release tool from the Real-Valued Breezy Addon release. The Breezy Purchaser a tool written in Python 3 which is designed such that a developer can provide a properly structured JSON file to generate Purchaser modules for any hero in the game. 

To start, the tool requires the following Python packages. Depending on your system, you may need to install them with `conda` or `pip`:

- simplejson
- pprint

To run the tool, place the `hero_data` folder into `ADDON_ROOT\scripts\vscripts\bots` and run `purchase_generator.py`. By default, it will generate a Shadow Fiend purchase module named `nevermore.lua`, and another for Invoker named `invoker.lua`.

## File Contents

The base functionality of Breezy Purchaser is spread across several Python 3 and Lua files, which you can view summaries of below. 

### Purchase Generator (purchase_generator.py)

This file holds the main logic for piecing together Purchaser modules. It first reads in the `purchaser.lua` module template file and then searches the `json` directory for various hero JSON files. Once those are found, it replaces important sections of the template with the data included in the JSON and then saves it to a Lua file in the `hero_data` folder with the same name as the JSON. These generated Lua modules create a Purchaser object and return it as a part of a `require` command, such as `local purchaser = require("bots/hero_data/nevermore")`.

### Data Handlers (data_handlers.py)

This file holds some utility functions for managing data files, including the code to open the template file, the code to get the JSON list from the `json` folder, and the code to convert a JSON array to a properly structured Lua table body.

### Code Handler (code_handler.py)

This file holds the logic for converting Breezy Purchaser Commands into Lua code. For more information on these commands, please see the "BP Commands" section below.

### Purchaser Template (purchaser.lua)

This file contains all of the code for a Purchaser module, but is not a runnable Lua file on its own. Some fields and function bodies are replaced with replacable codes (starting with `R_`) which are replaced as a part of the Purchase Generator program. See the "JSON Structure" section below for more information on the replacable codes.

## JSON Structure

### Replacable codes

The `purchaser.lua` files contains a selection of codes starting with `R_` which are replaced on tool execution. The list of codes and their purpose is found here:

* **R_HERO_TITLE_NAME**: The hero's name. Replaced in the module header comment. Mostly for book-keeping purposes.

* **R_HERO_NAME**: The hero's name, available as a class member. Replaced in a class string field.

* **R_ABILITY_COUNT**: The number of abilities in the included ability list. Replaced in a class number field.

* **R_ITEM_COUNT**: The number of items in the included item list. Replaced in a class number field.

* **R_ITEM_LIST**: The list of items to be purchased, in purchase order. Replaced in a class table field.

* **R_ABILITY_LIST**: The list of abilities to be learned in order. Replaced in a class table field.

* **R_MANAGE_INVENTORY_CODE**: The code body for managing the hero's inventory. This code is executed once before every item is purchased. Replaced in a class function body.

### JSON Configuration Example

The Breezy Purchaser JSON files contain two strings (one for the Title Name and the Hero Name), as well as three arrays (Items, Abilities, Code Commands). A simple example can be seen here:

```json
{
   "R_HERO_TITLE_NAME": "Commented Hero Name",
   "R_HERO_NAME": "Class Hero Name",   
   "R_ITEM_LIST": [
        "item_1",
        "item_2"
   ],
   "R_ABILITY_LIST": [
        "ability_1",
        "ability_2"
   ],
   "R_MANAGE_INVENTORY_CODE": [
        "command_1",
        "command_2"
   ]
}
```

The item count and ability count are not present in the JSON and are automatically generated in the Purchase Generator based on the lengths of the provided item and ability lists. 

Item names are added to the `R_ITEM_LIST` array as strings and must match the in-game item list available [here](https://developer.valvesoftware.com/wiki/Dota_2_Workshop_Tools/Scripting/Built-In_Item_Names) (probably incomplete). Ability names are done in the same fashion, with various sources for ability names being available. The most likely to be up-to-date can be found in the Dota 2 files at `dota 2 beta\game\dota\scripts\npc` in the `npc_heroes.txt` file. Valve is bad at making their current configurations public and every patch is almost certainly guaranteed to break something.

## BP Commands

The `R_MANAGE_INVENTORY_CODE` section of the JSON is designed to accept a list of strings fitting the structure of the Breezy Purchaser Commands style. 

Each command is a three-token string which contains a condition, an action, and an item name in the following format:

```condition action item_name```

The condition token creates an if statement in the generated Lua code based on the following condition tokens:

- **full**: if the hero's regular (6 slot) inventory is full, perform the action on the item.
- **fullall**: if the hero's inventory is full (inventory and backpack; 9 slots), perform the action on the item.
- **empty**: if the hero's inventory is empty, perform the action on the item. Currently not useful, but could be useful in later versions.
- **iX**: if the hero is currently holding X items, perform the action on the item.
- **pX**: if the next item to be purchased will be the Xth item purchase, perform the action on the item.

The action token signals performing some kind of action on the item in question based on the following action tokens:

- **use**: attempt to activate the named item.
- **remove**: attempts to delete the named item from the hero's inventory.
- **drop**: attempts to drop the named item at the hero's current position.

The item token can be any string, but ideally should be an item the hero is guaranteed to be holding.

### Examples

##### full remove item_branches

If the hero's inventory is currently full, the command will delete a single Iron Branch item from the hero's inventory. It generates the following code in the `ManageInventory()` function:

```lua
if self.HeroHandler:GetNumItemsInInventory() >= 6 then
	local item = self.HeroHandler:FindItemInInventory("item_branches")
	self.HeroHandler:RemoveItem(item)
end
```

##### empty drop item_branches

If the hero's inventory is currently empty, the command will attempt to drop a single Iron Branch item from the hero's inventory (which obviously doesn't work). It generates the following code in the `ManageInventory()` function:

```lua
if self.HeroHandler:GetNumItemsInInventory() == 0 then
	local item = self.HeroHandler:FindItemInInventory("item_branches")
	self.HeroHandler:DropItemAtPositionImmediate(item, self.HeroHandler:GetAbsOrigin())
end
```

##### i4 drop item_branches

If the hero's inventory has 4 items in it, the command will attempt to drop a single Iron Branch item from the hero's inventory. It generates the following code in the `ManageInventory()` function:

```lua
if self.HeroHandler:GetNumItemsInInventory() == 4 then
	local item = self.HeroHandler:FindItemInInventory("item_branches")
	self.HeroHandler:DropItemAtPositionImmediate(item, self.HeroHandler:GetAbsOrigin())
end
```

##### p5 remove item_branches

If the next item will be the 5th purchase this hero has made, the command will delete a single Iron Branch item from the hero's inventory. It generates the following code in the `ManageInventory()` function:

```lua
if self.CurrentItem == 5 then
	local item = self.HeroHandler:FindItemInInventory("item_branches")
	self.HeroHandler:RemoveItem(item)
end
```

## Notes

1. This tool hasn't been tested. Like, at all. If something breaks, I'll fix it. Don't be whiny.
2. This tool assumes you've entered all the correct fields and arrays to your JSON files. If you put in item/ability names that don't make sense, the client will just break (or, even worse, not break) and may not give you any indication that things are screwy. Be as accurate as possible and do some testing to make sure it's doing what you think it should.


